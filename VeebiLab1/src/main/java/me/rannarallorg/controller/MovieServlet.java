package me.rannarallorg.controller;

import me.rannarallorg.db.MovieDao;
import me.rannarallorg.model.Movie;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;


@WebServlet(urlPatterns = { "/movie/s" })
public class MovieServlet extends HttpServlet {

    private MovieDao movieDao = new MovieDao();
    private final static Logger logger = Logger.getLogger(String.valueOf(MovieServlet.class));

    public void init() {
        logger.info("MovieServlet.init(): initialized");
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        try {
            if (request.getParameterMap() .size() > 0) {
                if (request.getParameter("id") != null) {
                    logger.info("MovieServlet.doGet(): Search for movie by id");
                    getMovieById(request, response);
                }

                if (request.getSession().getAttribute("invalidMovie") != null) {
                    logger.info("MovieServlet.doGet(): Invalid movie found in session. Replacing session attribute");
                    request.setAttribute("movie", request.getSession().getAttribute("invalidMovie"));
                }
            } else {
                request.getSession().setAttribute("errors", null);
                request.getSession().setAttribute("invalidMovie", null);
                logger.info("MovieServlet.doGet(): Getting all movies");
                List<Movie> movies = movieDao.findAll();
                request.setAttribute("movies", movies);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        request.getRequestDispatcher("/movies.jsp").forward(request, response);
    }

    private void getMovieById(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        int id = getIdFromRequest(request);

        try {
            logger.info("MovieServlet.getMovieById(): Searching by id=" + id);
            Movie movie = movieDao.findById(id);

            if (movie == null) {
                logger.error("MovieServlet.getMovieById(): Couldn't found movie with id=" + id);
                request.getRequestDispatcher("/error.jsp").forward(request, response);
            }

            logger.info("MovieServlet.getMovieById(): Movie found. Adding to request.");
            request.setAttribute("putMovieInTable", true);
            request.setAttribute("movie", movie);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private int getIdFromRequest(HttpServletRequest request) {
        int id = -1;
        try {
            id = Integer.parseInt(request.getParameter("id"));
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        return id;
    }

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        if ("save".equals(request.getParameter("action"))) {

            HttpSession session = request.getSession();

            Movie movie = getMovieDataFromRequest(request);

            MovieValidator movieValidator = new MovieValidator();
            movieValidator.validate(movie);

            if (movieValidator.hasErrors()) {
                logger.info("MovieServlet.doPost(): Form had errors, redirecting back to form");
                session.setAttribute("errors", movieValidator.getErrorMap());
                session.setAttribute("invalidMovie", movie);
                response.sendRedirect("/movie/s?id=" + movie.getId());
            } else {
                try {
                    if (movieDao.update(movie)) {
                        logger.info("MovieServlet.doPost(): Update was successful");
                        response.sendRedirect("/movie/s");
                    }

                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }

    }

    private Movie getMovieDataFromRequest(HttpServletRequest request) {
        Movie movie = new Movie();

        String id = request.getParameter("id");
        movie.setId(id != null ? Integer.parseInt(id) : -1);

        movie.setName(request.getParameter("name"));
        movie.setGenre(request.getParameter("genre"));

        try {
            movie.setRating(Integer.parseInt(request.getParameter("rating")));
        } catch (NumberFormatException e) {
            movie.setRating(-1);
        }

        movie.setDescription(request.getParameter("description"));

        return movie;
    }
}