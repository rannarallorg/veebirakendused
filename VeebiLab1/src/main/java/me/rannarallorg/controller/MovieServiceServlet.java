package me.rannarallorg.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import me.rannarallorg.db.MovieDao;
import me.rannarallorg.model.Movie;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

/**
 * Created by rannar on 3/26/16.
 */

@WebServlet(urlPatterns = { "/movie/movieservice" })
public class MovieServiceServlet extends HttpServlet {

    private MovieDao movieDao = new MovieDao();
    private final static Logger logger = Logger.getLogger(String.valueOf(MovieServlet.class));

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        if (request.getParameter("id") != null) {
            try {
                Movie movie = movieDao.findById(Integer.parseInt(request.getParameter("id")));

                ObjectMapper mapper = new ObjectMapper();

                String jsonString = mapper.writeValueAsString(movie);

                response.setContentType("application/json");
                response.setCharacterEncoding("UTF-8");
                response.getWriter().write(jsonString);
            } catch (SQLException | NumberFormatException e) {
                e.printStackTrace();
            }
        }
    }
}
