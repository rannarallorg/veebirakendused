package me.rannarallorg.controller;

import me.rannarallorg.model.Movie;
import org.apache.commons.lang3.StringUtils;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by rannar on 3/26/16.
 */
public class MovieValidator {

    private Map<String, String> errorMap;

    public MovieValidator() {
        errorMap = new HashMap<>();
    }

    public void validate(Movie movie) {
        if (StringUtils.isBlank(movie.getName()) || movie.getName().length() > 30) {
            errorMap.put("name", "Nimi on tyhi");
        }

        if (StringUtils.isBlank(movie.getGenre()) || movie.getGenre().length() > 20) {
            errorMap.put("genre", "Zanr on tyhi");
        }

        if (StringUtils.isBlank(movie.getDescription())) {
            errorMap.put("description", "Kirjeldus on tyhi");
        }

        if (movie.getRating() < 0 || movie.getRating() > 5) {
            errorMap.put("rating", "Hinnang on tyhi");
        }
    }

    public Map<String, String> getErrorMap() {
        return errorMap;
    }

    public boolean hasErrors() {
        return errorMap.size() > 0;
    }
}
