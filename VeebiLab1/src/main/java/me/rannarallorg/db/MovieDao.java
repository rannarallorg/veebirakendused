package me.rannarallorg.db;
import me.rannarallorg.controller.MovieServlet;
import me.rannarallorg.model.Movie;
import org.apache.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class MovieDao {

    private static final String JDBC_DRIVER = "org.postgresql.Driver";
    private static final String DB_URL = "jdbc:postgresql://localhost:5432/veebirakendused";

    private static final String USER = "rannar";
    private static final String PASS = "rannar";

    private final static Logger logger = Logger.getLogger(String.valueOf(MovieServlet.class));

    public boolean update(Movie movie) throws SQLException {
        String updateQuery = "UPDATE movies SET name='" + movie.getName() +
                "', genre='" + movie.getGenre() +
                "', rating=" + movie.getRating() +
                ", description='" + movie.getDescription() +
                "' WHERE id=" + movie.getId();

        logger.info("MovieDao.update(): execute");
        Statement statement = executeQuery(updateQuery, false);
        return (statement != null && statement.getUpdateCount() > 0);
    }

    public List<Movie> findAll() throws SQLException {
        logger.info("MovieDao.findAll(): execute");
        Statement statement = executeQuery("SELECT * FROM movies", true);
        return readMoviesToList(statement.getResultSet());
    }

    public Movie findById(int id) throws SQLException {
        logger.info("MovieDao.findById(): execute");
        Statement statement = executeQuery("SELECT * FROM movies WHERE id=" + id, true);
        List<Movie> movies = readMoviesToList(statement.getResultSet());
        return movies != null ? movies.get(0) : null;
    }


    private List<Movie> readMoviesToList(ResultSet resultSet) throws SQLException {
        List<Movie> movies = new ArrayList<>();

            while (resultSet.next()) {
                Movie movie = new Movie();

                movie.setName(resultSet.getString(1));
                movie.setGenre(resultSet.getString(2));
                movie.setRating(Integer.parseInt(resultSet.getString(3)));
                movie.setDescription(resultSet.getString(4));
                movie.setId(Integer.parseInt(resultSet.getString(5)));

                movies.add(movie);
            }

        return movies.size() > 0 ? movies : null;
    }

    private Statement executeQuery(String query, boolean execute) throws SQLException {
        Connection connection;
        Statement statement = null;

        try {
            Class.forName(JDBC_DRIVER);

            connection = DriverManager.getConnection(DB_URL, USER, PASS);

            statement = connection.createStatement();

            if (execute) {
                statement.executeQuery(query);
            } else {
                statement.executeUpdate(query);
            }

        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        return statement;
    }
}