
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
    <title>Movies</title>

    <style>
        .movie-table th {
            text-align: center;
        }

        .form-container {
            display: none;
        }
    </style>
</head>
<body>

<nav class="navbar navbar-default">
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-brand" href="#">Veebirakendused</a>
        </div>
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li><a href="../logs/lab1.log" target="_blank">Logid</a></li>
                <li><a href="/movie/s">Servlet</a></li>
            </ul>
            <p class="navbar-text navbar-right">Rannar Allorg 134554IAPB</p>
        </div>
    </div>
</nav>

<div class="container-fluid">

    <c:choose>
        <c:when test="${requestScope.putMovieInTable}">
            <jsp:include page="movie_form.jsp" />
        </c:when>
        <c:otherwise>
            <jsp:include page="movie_table.jsp" />
        </c:otherwise>
    </c:choose>

</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.2/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>

<script src="../js/movie.js"></script>

</body>
</html>
