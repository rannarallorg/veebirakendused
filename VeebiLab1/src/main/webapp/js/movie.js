/**
 * Created by rannar on 3/27/16.
 */

$(".description-link").click(function() {
    var movieId = $(this).attr('id').split("-")[1];

    $.get( "/movie/movieservice?id=" + movieId, function( data ) {
        showDescription(data);
    }).fail(function() {
        alert("Midagi läks valesti");
    });
});

$("#hide-desc").click(function() {
   $(".form-container").hide();
});

function showDescription(data) {
    console.log(data.id);
    $("#id-value").text(data.id);
    $("#description").val(data.description);

    $(".form-container").show();
}