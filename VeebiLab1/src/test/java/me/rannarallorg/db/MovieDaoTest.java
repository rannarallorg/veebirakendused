package me.rannarallorg.db;

import me.rannarallorg.model.Movie;
import org.junit.Before;
import org.junit.Test;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 * Created by rannar on 3/26/16.
 */
public class MovieDaoTest {

    private static final String JDBC_DRIVER = "org.postgresql.Driver";
    private static final String DB_URL = "jdbc:postgresql://localhost:5432/veebirakendused";

    private static final String USER = "rannar";
    private static final String PASS = "rannar";

    private MovieDao movieDao;

    @Before
    public void setUp() {
        movieDao = new MovieDao();
    }

    @Test
    public void testFindAll() throws SQLException {
        List<Movie> movies = movieDao.findAll();

        assertTrue(movies.size() == 3);
        for (Movie m : movies) {
            assertNotNull(m.getName());
            assertNotNull(m.getGenre());
            assertNotNull(m.getDescription());
            assertTrue(m.getId() != 0);
            assertTrue(m.getRating() >= 0);
        }
    }

    @Test
    public void testFindById() throws SQLException, ClassNotFoundException {

        Movie movie = new Movie();
        movie.setName("asd");
        movie.setGenre("qwe");
        movie.setRating(1);
        movie.setDescription("lala");
        movie.setId(1);

        updateMovie(movie);

        Movie result = movieDao.findById(1);

        assertEquals(result.getId(), 1);
        assertEquals(result.getName(), "asd");
        assertEquals(result.getGenre(), "qwe");
        assertEquals(result.getRating(), 1);
        assertEquals(result.getDescription(), "lala");
    }


    @Test
    public void testUpdate() throws SQLException {
        Movie movie = new Movie();
        movie.setName("asd");
        movie.setGenre("qwe");
        movie.setRating(1);
        movie.setDescription("lala");
        movie.setId(1);

        movieDao.update(movie);

        Movie result = movieDao.findById(1);

        assertEquals(result.getId(), 1);
        assertEquals(result.getName(), "asd");
        assertEquals(result.getGenre(), "qwe");
        assertEquals(result.getRating(), 1);
        assertEquals(result.getDescription(), "lala");
    }

    private void updateMovie(Movie movie) throws SQLException, ClassNotFoundException {
        String updateQuery = "UPDATE movies SET name='" + movie.getName() +
                "', genre='" + movie.getGenre() +
                "', rating=" + movie.getRating() +
                ", description='" + movie.getDescription() +
                "' WHERE id=" + movie.getId();

        Connection connection;
        Statement statement;

        Class.forName(JDBC_DRIVER);

        connection = DriverManager.getConnection(DB_URL, USER, PASS);

        statement = connection.createStatement();

        statement.executeUpdate(updateQuery);
    }

}