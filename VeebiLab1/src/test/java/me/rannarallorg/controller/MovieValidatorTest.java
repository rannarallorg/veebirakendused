package me.rannarallorg.controller;

import me.rannarallorg.model.Movie;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by rannar on 3/28/16.
 */
public class MovieValidatorTest {

    @Test
    public void testValidateCorrect() {

        Movie movie = new Movie();
        movie.setId(5);
        movie.setName("Batman");
        movie.setGenre("Action");
        movie.setRating(5);
        movie.setDescription("lalalalala");

        MovieValidator validator = new MovieValidator();
        validator.validate(movie);

        assertFalse(validator.hasErrors());
    }

    @Test
    public void testValidateInvalidName() {

        Movie movie = new Movie();
        movie.setId(5);
        movie.setName("");
        movie.setGenre("Action");
        movie.setRating(5);
        movie.setDescription("lalalalala");

        MovieValidator validator = new MovieValidator();
        validator.validate(movie);

        assertTrue(validator.hasErrors());
        assertTrue(validator.getErrorMap().containsKey("name"));
    }

    @Test
    public void testValidateInvalidGenre() {

        Movie movie = new Movie();
        movie.setId(5);
        movie.setName("Batman");
        movie.setGenre("");
        movie.setRating(5);
        movie.setDescription("lalalalala");

        MovieValidator validator = new MovieValidator();
        validator.validate(movie);

        assertTrue(validator.hasErrors());
        assertTrue(validator.getErrorMap().containsKey("genre"));
    }

    @Test
    public void testValidateInvalidRating() {

        Movie movie = new Movie();
        movie.setId(5);
        movie.setName("Batman");
        movie.setGenre("lalala");
        movie.setRating(100);
        movie.setDescription("lalalalala");

        MovieValidator validator = new MovieValidator();
        validator.validate(movie);

        assertTrue(validator.hasErrors());
        assertTrue(validator.getErrorMap().containsKey("rating"));
    }

    @Test
    public void testValidateInvalidDescription() {

        Movie movie = new Movie();
        movie.setId(5);
        movie.setName("Batman");
        movie.setGenre("Action");
        movie.setRating(5);
        movie.setDescription("");

        MovieValidator validator = new MovieValidator();
        validator.validate(movie);

        assertTrue(validator.hasErrors());
        assertTrue(validator.getErrorMap().containsKey("description"));
    }

}