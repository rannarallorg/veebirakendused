--
-- PostgreSQL database dump
--

-- Dumped from database version 9.5.1
-- Dumped by pg_dump version 9.5.1

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: movies; Type: TABLE; Schema: public; Owner: rannar
--

CREATE TABLE movies (
    name character varying(30) NOT NULL,
    genre character varying(20),
    rating numeric(1,0),
    description text,
    id integer NOT NULL
);


ALTER TABLE movies OWNER TO rannar;

--
-- Name: movies_id_seq; Type: SEQUENCE; Schema: public; Owner: rannar
--

CREATE SEQUENCE movies_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE movies_id_seq OWNER TO rannar;

--
-- Name: movies_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: rannar
--

ALTER SEQUENCE movies_id_seq OWNED BY movies.id;


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: rannar
--

ALTER TABLE ONLY movies ALTER COLUMN id SET DEFAULT nextval('movies_id_seq'::regclass);


--
-- Data for Name: movies; Type: TABLE DATA; Schema: public; Owner: rannar
--

COPY movies (name, genre, rating, description, id) FROM stdin;
Spiderman123	Action	3	SPIDERMAAAAAN NANANANANAN	3
lotr1212312	Adventure	2	Frodo likes sam	2
asdas	qwe	2	lala	1
\.


--
-- Name: movies_id_seq; Type: SEQUENCE SET; Schema: public; Owner: rannar
--

SELECT pg_catalog.setval('movies_id_seq', 3, true);


--
-- Name: movies_pkey; Type: CONSTRAINT; Schema: public; Owner: rannar
--

ALTER TABLE ONLY movies
    ADD CONSTRAINT movies_pkey PRIMARY KEY (id);


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

