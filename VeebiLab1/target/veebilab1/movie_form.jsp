<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<div class="container">

    <c:if test="${not empty sessionScope.errors}">
        <div class="alert alert-danger">
            <strong>Viga!</strong> Salvestamine ebaõnnestus. Kontrolli, et kõik väljad oleksid täidetud.
        </div>
    </c:if>

    <form action="/movie/s?action=save" method="post">

        <div class="form-group">
            <input name="id" type="text" class="form-control" id="id" value="${requestScope.movie.id}" readonly>
        </div>

        <div class="form-group ${sessionScope.errors.containsKey("name") ? "has-error" : ""}">
            <label for="movieName">Nimi</label>
            <input name="name" type="text" class="form-control" id="movieName" placeholder="Nimi" value="${requestScope.movie.name}">
        </div>

        <div class="form-group ${sessionScope.errors.containsKey("genre") ? "has-error" : ""}">
            <label for="movieGenre">Žanr</label>
            <input name="genre" type="text" class="form-control" id="movieGenre" placeholder="Žanr" value="${requestScope.movie.genre}">
        </div>

        <div class="form-group ${sessionScope.errors.containsKey("rating") ? "has-error" : ""}">
            <label for="movieRating">Hinnang</label>
            <input name="rating" type="number" class="form-control" id="movieRating" placeholder="Hinnang" value="${requestScope.movie.rating eq -1 ? "" : requestScope.movie.rating}" min="0" max="5">
        </div>

        <div class="form-group ${sessionScope.errors.containsKey("description") ? "has-error" : ""}">
            <label for="movieDescription">Kirjeldus</label>
            <textarea name="description" class="form-control" rows="3" id="movieDescription" placeholder="Kirjeldus">${requestScope.movie.description}</textarea>
        </div>

        <button type="submit" class="btn btn-primary">Salvesta</button>
    </form>
</div>