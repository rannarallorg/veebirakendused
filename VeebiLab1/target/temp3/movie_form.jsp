<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<div class="container">

    <c:if test="${requestScope.hasErrors}">
        <div class="alert alert-danger">
            <strong>Viga!</strong> Salvestamine ebaõnnestus. Kontrolli, et kõik väljad oleksid täidetud.
        </div>
    </c:if>

    <form action="/movies/s" method="post">
        <div class="form-group">
            <label for="movieName">Nimi</label>
            <input name="name" type="text" class="form-control" id="movieName" placeholder="Nimi" value="${requestScope.movies[0].name}">
        </div>

        <div class="form-group">
            <label for="movieGenre">Žanr</label>
            <input name="genre" type="text" class="form-control" id="movieGenre" placeholder="Žanr" value="${requestScope.movies[0].genre}">
        </div>

        <div class="form-group">
            <label for="movieRating">Hinnang</label>
            <input name="rating" type="number" class="form-control" id="movieRating" placeholder="Hinnang" value="${requestScope.movies[0].rating}">
        </div>

        <div class="form-group">
            <label for="movieDescription">Kirjeldus</label>
            <textarea name="description" class="form-control" rows="3" id="movieDescription" placeholder="Kirjeldus">${requestScope.movies[0].description}</textarea>
        </div>

        <button type="submit" class="btn btn-primary">Salvesta</button>
    </form>
</div>