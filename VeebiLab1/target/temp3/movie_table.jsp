<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<table class="table table-striped movie-table">
    <thead>
    <tr class="text-center">
        <th></th>
        <th>Nimi</th>
        <th>Zanr</th>
        <th>Hinne</th>
        <th>Kirjeldus</th>
        <th></th>
    </tr>
    </thead>

    <tbody>
    <c:forEach items="${requestScope.movies}" var="movie">
        <tr class="text-center">
            <td><a href="?name=${movie.name}"><span class="glyphicon glyphicon-edit" aria-hidden="true"></span></a></td>
            <td><c:out value="${movie.name}"/></td>
            <td><c:out value="${movie.genre}"/></td>
            <td><c:out value="${movie.rating}"/></td>
            <td><c:out value="${movie.description}"/></td>
            <td><a href="#"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a></td>
        </tr>
    </c:forEach>
    </tbody>
</table>
